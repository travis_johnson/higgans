VERSIONS="$(readlink -m ${DIR}/../versions)"
HTTP="$(readlink -m ${DIR}/..)/httpdocs"
SUBMODULES="$(readlink -m ${DIR}/..)/submodules"

NC='\033[0m'
GREEN='\033[0;32m'
RED='\033[0;31m'
BLUE='\033[1;34m'
