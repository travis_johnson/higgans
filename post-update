#!/bin/sh
#
# This script will take whatever reference is being pushed ($TO) in the given branch ($BRANCH), and will check it out into a directory ($VERSIONS/date+time+$BRANCH+$TO), and symlink ($HTTP) to it once done.
GIT="${PWD}"
DIR="${PWD}/hooks"
. ${DIR}/info.inc

BRANCH=${1}
SHA="$(git show-ref ${BRANCH})"
SHA="${SHA%% *}"
STATIC="_${BRANCH##*/}_${SHA}"
DATETIME=$(date +'%Y-%m-%d_%H-%M')
EXISTS="$(find ${VERSIONS} -maxdepth 1 -name "*${SHA}" -print -quit)"
OUTDIR="${VERSIONS}/${DATETIME}${STATIC}"

_dorefs()
{
	echo "${GREEN}Deleting branch references.${NC}"
	echo "${BLUE}rm -rf \"${GIT}/refs/heads/\"*${NC}"
	rm -rf "${GIT}/refs/heads/"*
}

_exit()
{
	_dorefs
	exit $1
}

_dosubmodules()
{
	echo "${GREEN}Updating submodules.${NC}"
	echo "${BLUE}git cat-file -e \"${SHA}:.gitmodules\"${NC}"
	git cat-file -e "${SHA}:.gitmodules"
	if [ "$?" != "0" ]
	then
		echo "${GREEN}Nevermind! There are no submodules >.>${NC}"
	else
		echo "${BLUE}git cat-file -p \"${SHA}:.gitmodules\"${NC}"
		git cat-file -p "${SHA}:.gitmodules" |\
		sed -e 's~^\s*path = \(.*\)$~\1~' -e 's~^\s*url = \(.*\)$~\1~' -e '/^\[submodule /d' -e '/^\s*$/d' |\
		paste - - |\
		while read SUB WHERE
		do
			echo "${GREEN}Pulling down ${NC}${SUB}"
			echo "${BLUE}cd \"${DIR}/../../submodules${NC}"
			cd "${DIR}/../../submodules"
			[ -d "${SUB}" ] || echo "${BLUE}git clone --bare \"${WHERE}\" \"${SUB}\"${NC}" && git clone --bare "${WHERE}" "${SUB}"
			echo "${BLUE}cd \"${SUB}\"${NC}"
			cd "${SUB}"
			echo "${BLUE}git fetch${NC}"
			git fetch
			COMMIT=$(git --git-dir="`readlink -f ${DIR}/../repo`" ls-tree -rd "${SHA}" | sed -n -e "s~^160000 commit \(\S*\)\s\+${SUB}$~\1~p")
			echo "${GREEN}Extracting desired commit.${NC}"
			echo "${BLUE}git archive \"${COMMIT}\" | tar -xC \"${OUTDIR}/${SUB}\"${NC}"
			git archive "${COMMIT}" | tar -xC "${OUTDIR}/${SUB}"
		done
	fi
}

_dolinks()
{
	if [ -f "${DIR}/links" ]
	then
		echo "${GREEN}Creating needed links...${NC}"
		echo "${BLUE}cd \"${OUTDIR}\"${NC}"
		cd "${OUTDIR}"
		if [ "$?" != "0" ]
		then
			echo "${RED}Couldn't cd to the directory, permissions issue?${NC}"
			_exit 1
		fi
		echo "${BLUE}cat \"${DIR}/links\"${NC}"
		cat "${DIR}/links" |\
		while read LINK DEST
		do
			echo "${BLUE}ln -s \"${DEST}\" \"${LINK}\"${NC}"
			ln -s "${DEST}" "${LINK}"
			if [ "$?" != "0" ]
			then
				echo "${RED}Error creating one of the links.${NC}"
				_exit 1
			fi
		done
	fi
}

_doseds()
{
	if [ -f "${DIR}/seds" ]
	then
		echo "${BLUE}cd \"${OUTDIR}\"${NC}"
		cd "${OUTDIR}"

		echo "${GREEN}Sledding.${NC}"
		echo "${BLUE}cat ${DIR}/seds${NC}"
		cat "${DIR}/seds" |\
		while read FILE REPLACE WITH
		do
			echo "${BLUE}sed -e \"s~${REPLACE}~${WITH}~\" -i \"${FILE}\"${NC}"
			sed -e "s~${REPLACE}~${WITH}~" -i "${FILE}"
			if [ "$?" != "0" ]
			then
				echo ${RED}There was a rock in the snow.${NC}
				_exit 1
			fi
		done
	fi
}

_docommands()
{
	if [ -f "${DIR}/commands" ]
	then
		export DIR
		echo "${BLUE}cd \"${OUTDIR}\"${NC}"
		cd "${OUTDIR}"

		echo "${GREEN}Running extra commands.${NC}"
		echo "${BLUE}cat \"${DIR}/commands\"${NC}"
		cat "${DIR}/commands" |\
		while read CMD
		do
			echo "${BLUE}${CMD}${NC}"
			${CMD}
			if [ "$?" != "0" ]
			then
				echo "${RED}Error running one of the commands.${NC}"
				_exit 1
			fi
		done
	fi
}

_doaftercommands()
{
	if [ -f "${DIR}/after" ]
	then
		export DIR
		echo "${BLUE}cd \"${OUTDIR}\"${NC}"
		cd "${OUTDIR}"

		echo "${GREEN}Running 'after' commands.${NC}"
		echo "${BLUE}cat \"${DIR}/after\"${NC}"
		cat "${DIR}/after" |\
		while read CMD
		do
			echo "${BLUE}${CMD}${NC}"
			${CMD}
			if [ "$?" != "0" ]
			then
				echo "${RED}Error running one of the commands.${NC}"
				_exit 1
			fi
		done
	fi
}

_dorelic()
{
	if [ -f "${DIR}/nrcurl" ]
	then
		echo "${GREEN}Alerting New Relic to the deploy.${NC}"
		read NRKEY NRID < "${DIR}/nrcurl"
		echo "${BLUE}cd \"${DIR}/..\"${NC}"
		cd "${DIR}/.."
		echo "${BLUE}curl -H \"x-api-key:${NRKEY}\" -d \"deployment[application_id]=${NRID}\" -d \"deployment[revision]=${SHA}\" -d \"deployment[user]=$(git --no-pager show -s --format='%an')\" \"https://rpm.newrelic.com/deployments.xml\"${NC}"
		curl -H "x-api-key:${NRKEY}" -d "deployment[application_id]=${NRID}" -d "deployment[revision]=${SHA}" -d "deployment[user]=$(git --no-pager show -s --format='%an')" "https://rpm.newrelic.com/deployments.xml"
	fi
}

if [ -d "${EXISTS}" ]
then
	echo "${GREEN}This commit is already checked out.${NC}"
	OUTDIR="$(readlink -m ${EXISTS})"
	_docommands
	echo "${GREEN}Linking to existing directory.${NC}"
	echo "${BLUE}ln -sfn \"$(readlink -m ${EXISTS})\" \"${HTTP}\"${NC}"
	ln -sfn "${OUTDIR}" "${HTTP}"
	if [ "$?" != "0" ]
	then
		echo "${RED}Could not create the new link. Site may be down, check and alert your admin if so.${NC}"
		_exit 1
	fi
	_doaftercommands
	echo "${GREEN}Complete.${NC}"
elif [ -e "${EXISTS}" ]
then
	echo "${RED}The intended directory for this commit exists as not-a-directory, please fix!${NC}"
	_exit 1
elif [ -n "${EXISTS}" ]
then
	echo "${RED}No idea what just happened, envdump:\n$(env)${NC}\n"
	_exit 1
else
	echo "${GREEN}Creating output directory...${NC}"
	echo "${BLUE}mkdir \"${OUTDIR}\"${NC}"
	mkdir "${OUTDIR}"
	if [ "$?" != "0" ]
	then
		echo "${RED}Couldn't create directory for deploy.${NC}"
		_exit 1
	fi

	echo "${GREEN}Copying working tree...${NC}"
	echo "${BLUE}git archive \"${BRANCH}\" | tar -xC \"${OUTDIR}\"${NC}"
	git archive "${BRANCH}" | tar -xC "${OUTDIR}"
	if [ "$?" != "0" ]
	then
		echo "${RED}Couldn't extract commit to directory.${NC}"
		_exit 1
	fi

	_dosubmodules

	_dolinks

	_doseds

	_docommands

	echo "${GREEN}Linking to web root...${NC}"
	echo "${BLUE}ln -sfn \"$(readlink -m ${OUTDIR})\" \"${HTTP}\"${NC}"
	ln -sfn "$(readlink -m ${OUTDIR})" "${HTTP}"
	if [ "$?" != "0" ]
	then
		echo "${RED}Error creating new symlink. New deploy isn't up. Site may be down, check and alert your admin if so.${NC}"
		_exit 1
	fi

	_doaftercommands
	_dorelic

	echo "${GREEN}Deploy complete.${NC}"

	echo "${GREEN}Looking for old deploys to delete.${NC}"
	echo "${BLUE}ls \"${VERSIONS}\" | tail -n+4${NC}"
	ls -r "${VERSIONS}" | tail -n+4 |\
	while read OLD
	do
		echo "${BLUE}rm -rf \"${VERSIONS}/${OLD}\"${NC}"
		rm -rf "${VERSIONS}/${OLD}"
		if [ "$?" != "0" ]
		then
			echo "${RED}Error deleting old revision ${OLD%_*}${NC}"
		else
			echo "${GREEN}Revision ${OLD%_*} deleted.${NC}"
		fi
	done
	echo "${GREEN}No more revisions to delete.${NC}"

	_exit 0
fi

_exit 0
